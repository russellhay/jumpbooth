#pragma once

#include "ofMain.h"
#include "ofxOpenCv.h"
#include "ofxCvContourFinder.h"
#include "ofxBackground.h"
#include "ofxBackgroundLearningCompleteEvent.h"
#include "ofxKalmanFilter.h"
#include "ofxCameraKit.h"


#define WIDTH  640
#define HEIGHT 480
#define THRESHOLD 110
#define LOCATION 300
#define MIN_DURATION 5

class testApp : public ofBaseApp{

public:
    void setup();
    void update();
    void draw();
    
    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    
    void learningComplete(ofxBackgroundLearningCompleteEvent &arg);
    void onStill(ofxStillEvent &arg);
    void onDeviceEvent(ofxDeviceEvent &e);
    
private:
    ofVideoGrabber grabber;
    ofxCvContourFinder finder;
    ofxCvColorImage rgb;
    ofImage saved, loaded;
    ofxCvGrayscaleImage gray;
    ofxBackground   backgroundAddon;
    ofTrueTypeFont font;
    ofxKalmanFilter filter;
    ofxCvHaarFinder faceFinder;
    ofxCvBlob *blob;
    
    void drawLargestBlob();
    
    float triggerPosition;
    float verticalPosition;
    float lastTriggered;
    int threshold;
    bool learnBackground;
    bool learning;
    bool tracking;
    bool triggerPhoto;
    ofxCameraKit kit;
    ofxCameraDevice* activeDevice;
};
