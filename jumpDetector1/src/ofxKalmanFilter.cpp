//
//  ofxKalmanFilter.cpp
//  jumpDetector1
//
//  Created by Russell Hay on 10/28/13.
//
//

#include "ofxKalmanFilter.h"

ofEvent<ofxStillEvent> ofxStillEvent::events;
ofxStillEvent::ofxStillEvent(float height, float velocity, float acceleration) {
    _height = height;
    _velocity = velocity;
    _acceleration = acceleration;
}

float ofxStillEvent::getHeight() { return _height; }
float ofxStillEvent::getVelocity() { return _velocity; }
float ofxStillEvent::getAcceleration() { return _acceleration; }

ofxKalmanFilter::ofxKalmanFilter() :
    filter(4,4,0),
    measurement(4,1),
    prediction(4,1),
    estimated(4,1),
    ready(false),
    stopped(false)
{
    filter.transitionMatrix = *(cv::Mat_<float>(4,4) << 1,0,1,0,
                                                        0,1,0,1,
                                                        0,0,1,0,
                                                        0,0,0,1);
    measurement.setTo(cv::Scalar(0));
}

ofxKalmanFilter::~ofxKalmanFilter() { }

bool ofxKalmanFilter::readyToTrack() {
    return ready;
}

void ofxKalmanFilter::setInitial(const ofPoint &point) {
    filter.statePre.at<float>(0) = point.x;
    filter.statePre.at<float>(1) = point.y;
    filter.statePre.at<float>(2) = 0;
    filter.statePre.at<float>(3) = 0;
    cv::setIdentity(filter.measurementMatrix);
    cv::setIdentity(filter.processNoiseCov, cv::Scalar::all(1e-4));
    cv::setIdentity(filter.measurementNoiseCov, cv::Scalar::all(1e-1));
    cv::setIdentity(filter.errorCovPost, cv::Scalar::all(0.1));
    ready = true;
}

void ofxKalmanFilter::draw() {
    if (!ready) return;
    
    ofSetColor(255,0,0);
    ofCircle(getPrediction(), 10.0);
}

void ofxKalmanFilter::update(const ofPoint &point) {
    prediction = filter.predict();
    measurement(0) = point.x;
    measurement(1) = point.y;
    measurement(2) = point.x - previousMeasurement.x;
    measurement(3) = point.y - previousMeasurement.y;
    estimated = filter.correct(measurement);
    verticalPosition = estimated.at<float>(1);
    verticalVelocity = verticalPosition - previousPrediction.y;
    verticalAcceleration = verticalVelocity - previousVelocity;
    
    previousMeasurement = point;
    previousPrediction = getPrediction();
    
    previousPosition = verticalPosition;
    previousVelocity = verticalVelocity;
    previousAcceleration = verticalAcceleration;
    
    if (stopped && verticalAcceleration > 0) {
        stopped = false;
    }

    if (verticalAcceleration < 0 && !stopped) {
        ofxStillEvent event(verticalPosition, verticalPosition, verticalAcceleration);
        ofNotifyEvent(ofxStillEvent::events, event);
        stopped = true;
    }


}

const ofPoint ofxKalmanFilter::getPrediction() {
    ofPoint point;
    point.x = estimated.at<float>(0);
    point.y = estimated.at<float>(1);
    
    return point;
}