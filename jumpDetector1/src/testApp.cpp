#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
  
    ofBackground(0,0,0);
    ofSetFrameRate(40);
    threshold = THRESHOLD;
    triggerPosition = LOCATION;
    lastTriggered = 0;
    
    font.loadFont("Walkway_Bold.ttf", 32);
    
    grabber.initGrabber(WIDTH, HEIGHT, true);
    backgroundAddon.allocate(WIDTH, HEIGHT);
    ofAddListener(ofxBackgroundLearningCompleteEvent::events, this, &testApp::learningComplete);
    
    learnBackground = true;
    learning = true;
    blob = NULL;
    tracking = false;
    triggerPhoto = false;
    
    rgb.allocate(WIDTH, HEIGHT);
    saved.allocate(WIDTH, HEIGHT, OF_IMAGE_COLOR);
    gray.allocate(WIDTH, HEIGHT);
    saved.clear();
    
    ofAddListener(ofxStillEvent::events, this, &testApp::onStill);
    activeDevice = NULL;
//    kit.setDownloadFolder("captures");
    kit.listCameras();
    
    ofAddListener(kit.deviceEvents, this, &testApp::onDeviceEvent);
}

//--------------------------------------------------------------
void testApp::update(){
    grabber.update();
    if (grabber.isFrameNew()) {
        rgb.setFromPixels(grabber.getPixels(), WIDTH, HEIGHT);
        rgb.flagImageChanged();
        rgb.blurGaussian();
        backgroundAddon.update(rgb);

        gray = backgroundAddon.backgroundCodeBookConnectedComponents;
        finder.findContours(gray, 1000, 99999, 1, false);
        blob = &finder.blobs[0];
    }
    
    if (learnBackground) {
        backgroundAddon.startLearning();
        learnBackground = false;
        learning = true;
    }
    
    if (blob == NULL) return;
    
    if (tracking) {
        tracking = false;
        filter.setInitial(blob->centroid);
    } else if (filter.readyToTrack()) {
        filter.update(blob->centroid);
    }
    
    if (triggerPhoto) {
        triggerPhoto = false;
        if (activeDevice != NULL) {
            activeDevice->takePicture();
        }
        saved.setFromPixels(grabber.getPixels(), WIDTH, HEIGHT, OF_IMAGE_COLOR);
        saved.saveImage("captured.png");
    }

}

//--------------------------------------------------------------
void testApp::draw(){
    ofSetColor(255, 255, 255);
    if (learning) {
        font.drawString("Learning Background", 10, 32);
        return;
    }
    grabber.draw(0,0);
    grabber.draw(0, HEIGHT);
    if (saved.isAllocated())
        saved.draw(WIDTH, 0, WIDTH, HEIGHT);
    if (loaded.isAllocated())
        loaded.draw(WIDTH, HEIGHT, WIDTH, HEIGHT);
    drawLargestBlob();
    faceFinder.draw(0,HEIGHT);
}

void testApp::drawLargestBlob() {
    if (blob == NULL) return;
    
    
    ofNoFill();
    ofSetColor(255, 0, 0);
    blob->draw();
    

    ofCircle(blob->centroid, 10.0);
    ofSetColor(255,255, 0);
    if (triggerPhoto) {
        ofFill();
    }
    filter.draw();
    ofSetColor(255);
    ofLine(0, triggerPosition, WIDTH, triggerPosition);
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){
    switch (key) {
        case OF_KEY_UP:
            threshold = threshold<255 ? threshold + 1 : threshold;
            break;
        case OF_KEY_DOWN:
            threshold = threshold>0 ? threshold-1: threshold;
            break;
        case OF_KEY_RETURN:
            learnBackground = true;
            break;
    }

}

void testApp::onStill(ofxStillEvent &arg) {
    verticalPosition = arg.getHeight();
    if (verticalPosition < triggerPosition && ofGetElapsedTimef() - lastTriggered > MIN_DURATION) {
        triggerPhoto = true;
        lastTriggered = ofGetElapsedTimef();
    }
}

void testApp::learningComplete(ofxBackgroundLearningCompleteEvent &args) {
    learning = false;
    tracking = true;
}

void testApp::onDeviceEvent(ofxDeviceEvent &e) {
    switch(e.eventType) {
        case ofxCameraKit::CK_ALL_CAMERAS_FOUND:
            if (kit.devices.size() > 0) {
                activeDevice = kit.devices[0];
                activeDevice->openDevice();
                activeDevice->enableTethering();
                ofLog(OF_LOG_NOTICE, activeDevice->name);
            }
            break;
            
        case ofxCameraKit::CK_CAMERA_REMOVED:
            if (activeDevice == e.device->device)
                activeDevice = NULL;
            break;
            
        case ofxCameraKit::CK_CAMERA_READY:
            e.device->enableTethering();
            break;
            
        case ofxCameraKit::CK_PHOTO_DOWNLOADED:
            loaded.loadImage(e.photoPath);
            break;
    }
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 

}
