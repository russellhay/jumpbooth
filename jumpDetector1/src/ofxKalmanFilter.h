//
//  ofxKalmanFilter.h
//  jumpDetector1
//
//  Created by Russell Hay on 10/28/13.
//
//

#pragma once

#include <ofMain.h>
#include <opencv2/opencv.hpp>
#include <ofEvents.h>

class ofxStillEvent : public ofEventArgs {
public:
    ofxStillEvent(float height, float velocity, float acceleration);
    float getHeight();
    float getVelocity();
    float getAcceleration();
    
    static ofEvent<ofxStillEvent> events;
private:
    float _height;
    float _velocity;
    float _acceleration;
};

class ofxKalmanFilter : public ofBaseDraws {
public:
    ofxKalmanFilter();
    virtual ~ofxKalmanFilter();
    
    void setInitial(const ofPoint &point);
    void update(const ofPoint &point);
    const ofPoint getPrediction();
    
    virtual void draw(float x, float y, float w, float h) { };
    virtual void draw(float x, float y) { };
    virtual void draw();
    virtual float getWidth() { return 0; }
    virtual float getHeight() { return 0; }
    
    bool readyToTrack();
    
protected:
    cv::KalmanFilter filter;
    cv::Mat_<float> measurement;
    cv::Mat_<float> prediction;
    cv::Mat_<float> estimated;
    bool ready, stopped;
    ofPoint previousMeasurement;
    ofPoint previousPrediction;
    float verticalPosition, verticalVelocity, verticalAcceleration;
    float previousPosition, previousVelocity, previousAcceleration;
    
};
